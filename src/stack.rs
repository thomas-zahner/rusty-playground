fn main() {
    assert_eq!(is_valid(""), true);
    assert_eq!(is_valid("nonsense"), false);

    assert_eq!(is_valid("("), false);
    assert_eq!(is_valid("}"), false);

    assert_eq!(is_valid("()"), true);
    assert_eq!(is_valid("[]"), true);
    assert_eq!(is_valid("{}"), true);

    assert_eq!(is_valid("{[()()][()()]}"), true);
    assert_eq!(is_valid("[(){[]}]"), true);

    assert_eq!(is_valid("())"), false);
    assert_eq!(is_valid("[[]"), false);
    assert_eq!(is_valid("[(])"), false);
}

enum BracketDirection {
    Opening,
    Closing,
    Invalid,
}

#[derive(PartialEq)]
enum BracketType {
    Round,
    Square,
    Curly,
    Invalid,
}

fn is_valid(bracket_sequence: &str) -> bool {
    let mut stack: Vec<char> = Vec::new();

    for current in bracket_sequence.chars() {
        use BracketDirection::*;
        match bracket_direction(current) {
            Opening => stack.push(current),
            Closing => {
                if !is_valid_closing_bracket(stack.pop(), current) {
                    return false;
                }
            }
            Invalid => return false,
        }
    }

    if stack.len() > 0 { false } else { true }
}

fn is_valid_closing_bracket(opening_bracket: Option<char>, closing_bracket: char) -> bool {
    return match opening_bracket {
        None => false,
        Some(opening_bracket) => {
            bracket_type(opening_bracket) == bracket_type(closing_bracket)
        }
    };
}

fn bracket_type(character: char) -> BracketType {
    use BracketType::*;
    return match character {
        '(' | ')' => Round,
        '[' | ']' => Square,
        '{' | '}' => Curly,
        _ => Invalid,
    };
}

fn bracket_direction(character: char) -> BracketDirection {
    use BracketDirection::*;
    return match character {
        '(' | '[' | '{' => Opening,
        ')' | ']' | '}' => Closing,
        _ => Invalid,
    };
}
