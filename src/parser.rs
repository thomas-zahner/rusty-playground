use core::iter::Peekable;
use core::str::Chars;
use std::vec::IntoIter;

fn calc(expr: &str) -> f64 {
    let mut tokens: Peekable<IntoIter<Token>> = tokenise(expr)
        .into_iter()
        .filter(|t| t != &Token::Whitespace)
        .collect::<Vec<_>>()
        .into_iter()
        .peekable();
    parse(&mut tokens).evaluate()
}

fn parse(tokens: &mut Peekable<IntoIter<Token>>) -> Expression {
    let mut expression = parse_smallest(tokens);

    while let Some(token) = tokens.next() {
        match token {
            Token::Operator(op) => {
                let left = Box::new(expression);
                let right = Box::new(parse_smallest(tokens));
                //if op == Operation::Mul { todo!("{:?} {:?}", left, right) }
                expression = Expression::Op(left, op, right);
            },
            other => panic!("Expected either nothing or an operator after an expression, but encountered {other:?}"),
        }
    }

    expression
}

fn parse_smallest(tokens: &mut Peekable<IntoIter<Token>>) -> Expression {
    use Token::*;
    let expression = match tokens.next().expect("Expected expression") {
        Number(n) => Expression::Number(n),
        Operator(Operation::Sub) => match tokens.peek() {
            Some(Number(_)) => Expression::Negate(Box::new(parse_smallest(tokens))),
            _ => Expression::Negate(Box::new(parse(tokens))),
        },
        Paranthesis(ParanthesisType::Opening) => {
            let mut opened = 1;
            let mut inner_tokens = vec![];

            loop {
                let token = tokens.next().expect("Unclosed paranthesis in expression");
                if token == Paranthesis(ParanthesisType::Opening) {
                    opened += 1;
                }
                if token == Paranthesis(ParanthesisType::Closing) {
                    opened -= 1;
                }
                if opened == 0 {
                    break;
                }
                inner_tokens.push(token);
            }

            parse(&mut inner_tokens.into_iter().peekable())
        }
        Paranthesis(ParanthesisType::Closing) => panic!("Unexpected closing paranthesis"),
        Whitespace => unreachable!(),
        token => panic!("Unexpected token {token:?}"),
    };

    match tokens.peek() {
        Some(Operator(Operation::Mul)) => {
            tokens.next();
            let right = Box::new(parse_smallest(tokens));
            Expression::Op(Box::new(expression), Operation::Mul, right)
        }
        Some(Operator(Operation::Div)) => {
            tokens.next();
            let right = Box::new(parse_smallest(tokens));
            Expression::Op(Box::new(expression), Operation::Div, right)
        }
        _ => expression,
    }
}

#[derive(Debug)]
enum Expression {
    Number(f64),
    Op(Box<Expression>, Operation, Box<Expression>),
    Negate(Box<Expression>),
}

impl Expression {
    fn evaluate(&self) -> f64 {
        use Expression::*;
        match dbg!(self) {
            Number(n) => *n,
            Negate(e) => -e.evaluate(),
            Op(left, operation, right) => {
                let left = left.evaluate();
                let right = right.evaluate();
                match operation {
                    Operation::Add => left + right,
                    Operation::Sub => left - right,
                    Operation::Mul => left * right,
                    Operation::Div => left / right,
                }
            }
        }
    }
}

fn tokenise(expr: &str) -> Vec<Token> {
    use Operation::*;
    use ParanthesisType::*;
    use Token::*;

    let mut iter = expr.chars().peekable();
    let mut tokens = vec![];

    while let Some(c) = iter.next() {
        let token = match c {
            c if c.is_whitespace() => consume_whitespace(&mut iter),
            c if c.is_digit(10) => consume_number(c, &mut iter),
            '+' => Operator(Add),
            '-' => Operator(Sub),
            '*' => Operator(Mul),
            '/' => Operator(Div),
            '(' => Paranthesis(Opening),
            ')' => Paranthesis(Closing),
            c => panic!("Unexpected character encountered: '{}'", c),
        };
        tokens.push(token);
    }

    tokens
}

fn consume_whitespace(iter: &mut Peekable<Chars>) -> Token {
    consume_while(|c| c.is_whitespace(), iter);
    Token::Whitespace
}

fn consume_number(first: char, iter: &mut Peekable<Chars>) -> Token {
    let mut string: String = first.into();
    string.push_str(&consume_while(|c| c.is_digit(10) || c == '.', iter));
    let parsed: f64 = string.parse().expect(&format!("Invalid number '{string}'"));
    Token::Number(parsed)
}

fn consume_while(predicate: fn(char) -> bool, iter: &mut Peekable<Chars>) -> String {
    let mut string = String::new();
    while let Some(c) = iter.peek() {
        if predicate(*c) {
            string.push(*c);
            iter.next();
        } else {
            break;
        }
    }
    string
}

#[derive(Debug, PartialEq)]
enum Token {
    Whitespace,
    Number(f64),
    Operator(Operation),
    Paranthesis(ParanthesisType),
}

#[derive(Debug, PartialEq, Clone)]
enum Operation {
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Debug, PartialEq)]
enum ParanthesisType {
    Opening,
    Closing,
}

#[cfg(test)]
mod tests {
    use super::calc;

    // Wrap custom message to reduce repitition
    macro_rules! assert_expr_eq {
        ($expr: expr, $expect: expr) => {
            assert_eq!(
                calc($expr),
                $expect,
                "\nexpected expression \"{}\" to equal \"{:?}\", but got \"{:?}\"",
                $expr,
                $expect,
                calc($expr),
            );
        };
    }

    #[test]
    fn tokenise_test() {
        use crate::tokenise;
        use crate::Operation::*;
        use crate::ParanthesisType::*;
        use crate::Token::*;

        assert_eq!(tokenise(" \n\t  "), vec![Whitespace]);
        assert_eq!(
            tokenise("+-/*"),
            vec![Operator(Add), Operator(Sub), Operator(Div), Operator(Mul)]
        );
        assert_eq!(
            tokenise("()"),
            vec![Paranthesis(Opening), Paranthesis(Closing)]
        );
        assert_eq!(tokenise("1.23"), vec![Number(1.23)]);
        assert_eq!(tokenise("3457"), vec![Number(3457f64)]);
        assert_eq!(
            tokenise("-(3 +2.1)"),
            vec![
                Operator(Sub),
                Paranthesis(Opening),
                Number(3.0),
                Whitespace,
                Operator(Add),
                Number(2.1),
                Paranthesis(Closing)
            ]
        );
    }

    #[test]
    fn single_values() {
        assert_expr_eq!("0", 0.0);
        assert_expr_eq!("1", 1.0);
        assert_expr_eq!("42", 42.0);
        assert_expr_eq!("350", 350.0);
    }

    #[test]
    fn basic_operations() {
        assert_expr_eq!("1 + 1", 2.0);
        assert_expr_eq!("1 - 1", 0.0);
        assert_expr_eq!("1 * 1", 1.0);
        assert_expr_eq!("1 / 1", 1.0);
        assert_expr_eq!("12 * 123", 1476.0);
    }

    #[test]
    fn whitespace_between_operators_and_operands() {
        assert_expr_eq!("1-1", 0.0);
        assert_expr_eq!("1 -1", 0.0);
        assert_expr_eq!("1- 1", 0.0);
        assert_expr_eq!("1* 1", 1.0);
    }

    #[test]
    fn unary_minuses() {
        assert_expr_eq!("1- -1", 2.0);
        assert_expr_eq!("1--1", 2.0);
        assert_expr_eq!("1 - -1", 2.0);
        assert_expr_eq!("-42", -42.0);
    }

    #[test]
    fn parentheses() {
        assert_expr_eq!("(1)", 1.0);
        assert_expr_eq!("((1))", 1.0);
        assert_expr_eq!("((80 - (19)))", 61.0);
    }

    #[test]
    fn negate() {
        assert_expr_eq!("(-5 + 2)", -3.0);
    }

    #[test]
    fn associativity() {
        //assert_expr_eq!("2*2+1", 5.0); // operators are always evaluated from left-to-right
        //assert_expr_eq!("2*(2+1)", 6.0);
        //assert_expr_eq!("2+3*4", 2.0+3.0*4.0); // * and / have precedence over + and -
        //assert_expr_eq!("2+(1)*(2)", 4.0);
        //assert_expr_eq!("2 /2+3 * 4.75- -6", 21.25);

        //assert_expr_eq!("2 / 2 * 4", 4.0); // * and / are still evaluated from left-to-right
    }

    #[test]
    fn multiple_operators() {
        assert_expr_eq!("1 - -(-(-(-4)))", -3.0);
        assert_expr_eq!("(1 - 2) + -(-(-(-4)))", 3.0);
        //assert_expr_eq!("12* 123/(-5 + 2)", -492.0);
        //assert_expr_eq!("2 / (2 + 3) * 4.33 - -6", 7.732);
        //assert_expr_eq!("((2.33 / (2.9+3.5)*4) - -6)", 7.45625);
        //assert_expr_eq!("2 /2+3 * 4.75- -6", 21.25);
    }
}
