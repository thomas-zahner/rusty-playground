use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::time::Duration;
use std::time::Instant;

/// A quick attempt to implement very basic string matching algorithms.
/// The performance of both implementations seem to be pretty bad, especially compared to Rust's
/// native implementation. It's interesting to note that Boyer Moore's algorithm performs better
/// in the unoptimized build but in the release build it performs even worse than the brute force
/// implementation. Maybe my implementation of Boyer Moore's is wrong...
fn main() {
    let text = &get_lorem_ipsum();
    let search = &"vehicula".to_string();

    measure_performance(&brute_force_find, "Brute force", text, search);
    measure_performance(&boyer_moore_find, "Boyer moore", text, search);
    measure_performance(&str::find, "Native", text, search);
}

fn get_lorem_ipsum() -> String {
    let mut file = File::open("res/lorem-ipsum.txt").unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents);
    contents
}

fn measure_performance<'a>(find_algorithm: &dyn Fn(&'a str, &'a str) -> Option<usize>,
                           algorithm_name: &str,
                           text: &'a String, pattern: &'a String) {
    let before = Instant::now();
    let result: Option<usize> = find_algorithm(&text, pattern);

    println!("{} algorithm took {:?}. Result: {:?}", algorithm_name, before.elapsed(), result);
}

fn brute_force_contains(text: &str, pattern: &str) -> bool {
    brute_force_find(pattern, text).is_some()
}

fn brute_force_find(text: &str, pattern: &str) -> Option<usize> {
    let text_chars: Vec<char> = text.chars().map(|char| char).collect();
    let pattern_chars: Vec<char> = pattern.chars().map(|char| char).collect();

    for text_position in 0..text_chars.len() {
        for pattern_position in 0..pattern_chars.len() {
            if pattern_chars[pattern_position] != text_chars[text_position + pattern_position] {
                break;
            }

            if pattern_position == pattern.len() - 1 {
                return Some(text_position);
            }
        }
    }

    None
}

fn last_occurrence(pattern: &str) -> HashMap<char, usize> {
    let mut map: HashMap<char, usize> = HashMap::new();
    for (index, char) in pattern.chars().enumerate() {
        map.insert(char, index);
    }

    map
}

fn boyer_moore_contains(text: &str, pattern: &str) -> bool {
    boyer_moore_find(pattern, text).is_some()
}

fn boyer_moore_find(text: &str, pattern: &str) -> Option<usize> {
    let text_chars: Vec<char> = text.chars().map(|char| char).collect();
    let pattern_chars: Vec<char> = pattern.chars().map(|char| char).collect();

    if pattern_chars.len() > text_chars.len() { return None; }

    let last_occurrence_in_pattern = last_occurrence(pattern);

    let mut text_position = pattern_chars.len() - 1;

    while text_position < text_chars.len() {
        let mut step = 1;

        for offset in 0..pattern_chars.len() {
            let current_pattern_char = pattern_chars[pattern_chars.len() - 1 - offset];
            let current_text_char = text_chars[text_position - offset];

            if current_pattern_char != current_text_char {
                match last_occurrence_in_pattern.get(&current_text_char) {
                    None => step = pattern_chars.len(),
                    Some(position) => {
                        step = pattern_chars.len() - 1 - position;
                        if step == 0 { step = 1; }
                    }
                }

                break;
            }

            if offset == pattern_chars.len() - 1 { return Some(text_position - offset); }
        }

        text_position += step;
    }

    None
}
